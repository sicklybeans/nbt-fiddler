/// TODO: Timing tests and improvements.

pub struct PackedLongArray {
  data: Vec<i64>,
  bits_per_index: usize,
  length: usize,
}

impl PackedLongArray {

  pub fn to_string(&self) -> String {
    format!("byte length: {}, bits_per_index: {}, length: {}", self.data.len() * 8, self.bits_per_index, self.length)
  }

  pub fn new_with_length(data: Vec<i64>, length: usize) -> PackedLongArray {
    let bits_per_index = ((data.len() as f32 * 64.0) / (length as f32)).floor() as usize;
    let p = PackedLongArray { data, bits_per_index, length };
    if 64 * p.data.len() / p.bits_per_index < p.length {
      eprintln!("for packed array: {}", p.to_string());
      eprintln!("real computed length: {}", 64 * p.data.len() / p.bits_per_index);
      panic!("This should never happen");
    }
    p
  }

  pub fn new_with_bit_size(data: Vec<i64>, bits_per_index: usize) -> PackedLongArray {
    let length = ((data.len() as f32 * 64.0) / (bits_per_index as f32)) as usize;
    PackedLongArray { data, bits_per_index, length }
  }

  pub fn from_bytes(data: &[u8], bits_per_index: usize) -> PackedLongArray {

    let mut values = Vec::new();
    let mut dst: i64 = 0;
    let mut dst_bit_offset: i64 = 0;
    let bpi: i64 = bits_per_index as i64;

    for d in data.iter() {
      let dst_bits_left = 64 - dst_bit_offset;
      let leftover = dst_bits_left - bpi;

      if leftover >= 0 {
        dst += (*d as i64) << leftover;
        dst_bit_offset += bpi;
        if dst_bit_offset == 64 {
          values.push(dst);
          dst_bit_offset = 0;
          dst = 0;
        }
      } else {
        // let part1 = d >> (-leftover);
        dst += (*d as i64) >> (-leftover);
        values.push(dst);
        dst_bit_offset = -leftover;
        dst = (*d as i64) << (64 + leftover);
      }
    }
    if dst_bit_offset != 0 {
      values.push(dst);
    }

    Self::new_with_bit_size(values, bits_per_index)
  }

  pub fn into_longs(self) -> Vec<i64> {
    self.data
  }

  pub fn byte_length(&self) -> usize {
    self.data.len() * 8
  }

  pub fn len(&self) -> usize {
    self.length
  }

  pub fn search(&self, search_value: u8) -> Option<usize> {
    for index in 0..self.length {
      if self.read(index) == search_value {
        return Some(index)
      }
    }
    None
  }

  pub fn read(&self, index: usize) -> u8 {

    let src_bit_index = index * self.bits_per_index;
    let src_index = src_bit_index / 64;
    let src_bit_offset = src_bit_index % 64;

    let bits_per_index = self.bits_per_index;
    let src_bits_left = 64 - src_bit_offset;
    let value = self.data[src_index];

    // This is number of bits remaining in current byte after entire value is read (can be neg)
    let leftover = (src_bits_left as i64) - (bits_per_index as i64);

    // If the offset is positive, the current byte contains enough info to fill request.
    if leftover >= 0 {
      ((value >> leftover) & Self::bit_mask(bits_per_index)) as u8
    } else {
      let part1 = (value & Self::bit_mask(src_bits_left)) << (-leftover);
      let part2 = ((self.data[src_index + 1]) >> (64 + leftover)) & Self::bit_mask((-leftover) as usize);
      (part1 | part2) as u8
    }
  }

  pub fn write(&mut self, index: usize, value: u8) {
    let arr_bit_index = index * self.bits_per_index;
    let data_index = arr_bit_index / 64;
    let bit_offset = arr_bit_index % 64;

    let bits_per_index = self.bits_per_index as i64;
    let data_bits_left = 64 - bit_offset as i64;

    // Number of bits remaining after entire value is pulled from current value (can be neg)
    let leftover = data_bits_left - bits_per_index;

    if leftover >= 0 {
      // If leftover is positive, entire target position is contained within single index
      let new_mask = Self::bit_mask(self.bits_per_index) << leftover;
      let old_mask = !new_mask;
      let target = (value as i64) << leftover;
      self.data[data_index] = (self.data[data_index] & old_mask) | (target & new_mask);
    } else {
      let remaining_bits = -leftover;

      // Part 1 (modify first value that needs to change)
      let old_mask = !Self::bit_mask((bits_per_index - remaining_bits) as usize);
      let target = (value as i64) >> remaining_bits;
      self.data[data_index] = (self.data[data_index] & old_mask) | target;

      // Part 2 (modify spillover value)
      let new_mask = Self::bit_mask(remaining_bits as usize);
      let old_mask = !(new_mask << (64 - remaining_bits));
      let target = ((value as i64) & new_mask) << (64 - remaining_bits);
      self.data[data_index + 1] = (self.data[data_index + 1] & old_mask) | target;
    }
  }

  fn bit_mask(size: usize) -> i64 {
    if size < 64 {
      (1 << size) - 1
    } else {
      panic!("Cannot obtain a bit mask of size greater than 8 ({})", size);
    }
  }
}

#[cfg(test)]
mod packed_long_array_tests {
  use super::PackedLongArray;

  #[test]
  fn it_can_construct_packed_values_with_8_bits_per_value() {
    let bit_size = 8;
    let original = vec![1, 19, 18, 31, 26, 5, 0, 3, 7, 45, 3];
    let packed = PackedLongArray::from_bytes(&original, bit_size);
    assert_eq!(packed.data.len(), 2);
    assert_eq!(packed.data[0], (1 << 56) + (19 << 48) + (18 << 40) + (31 << 32) + (26 << 24) + (5 << 16) + (0 << 8) + 3);
  }

  #[test]
  fn it_can_construct_packed_values_with_8_bits_per_value_with_trailing0() {
    let bit_size = 8;
    let original = vec![1, 2, 3, 4, 5, 6, 7, 8, 0];
    let packed = PackedLongArray::from_bytes(&original, bit_size);
    assert_eq!(packed.data.len(), 2);
  }

  #[test]
  fn it_can_construct_packed_values_with_5_bits_per_value() {
    let bit_size = 5;
    let original = vec![1, 19, 18, 31, 26, 5, 0, 3, 7, 15, 3];
    let packed = PackedLongArray::from_bytes(&original, bit_size);
    // assert_eq!(packed.data.len(), 1);
    assert_eq!(packed.data[0],
      (1 << 59) +
      (19 << 54) +
      (18 << 49) +
      (31 << 44) +
      (26 << 39) +
      (5 << 34) +
      (0 << 29) +
      (3 << 24) +
      (7 << 19) +
      (15 << 14) +
      (3 << 9));
  }

  #[test]
  fn it_can_read_packed_values_with_5_bits_per_value() {
    let bit_size = 5;
    let original = vec![1, 19, 18, 31, 26, 5, 0];
    let packed = PackedLongArray::from_bytes(&original, bit_size);

    for i in 0..original.len() {
      assert_eq!(packed.read(i), original[i]);
    }
  }

  #[test]
  fn it_can_read_packed_values_with_7_bits_per_value() {
    let bit_size = 7;
    let original = vec![1, 127, 18, 31, 26, 5, 85, 127, 0];
    let packed = PackedLongArray::from_bytes(&original, bit_size);

    for i in 0..original.len() {
      assert_eq!(packed.read(i), original[i]);
    }
    assert_eq!(packed.byte_length(), 8);
    assert_eq!(packed.len(), 9);
  }

  #[test]
  fn it_can_read_packed_values_with_8_bits_per_value() {
    let bit_size = 8;
    let original = vec![255, 128, 127, 64, 63, 31, 15, 7, 3];
    let packed = PackedLongArray::from_bytes(&original, bit_size);

    for i in 0..original.len() {
      assert_eq!(packed.read(i), original[i]);
    }
  }

  #[test]
  fn it_can_write_packed_values_with_4_bits_per_value() {
    let bit_size = 4;
    let original = vec![15, 13, 11, 9, 7, 5, 3, 1, 2];
    let changes = vec![(0, 13), (2, 7), (3, 0), (8, 3)];
    let expected = vec![13, 13, 7, 0, 7, 5, 3, 1, 3];

    let mut packed = PackedLongArray::from_bytes(&original, bit_size);
    for (i, value) in changes.iter() {
      packed.write(*i, *value);
    }

    for i in 0..expected.len() {
      assert_eq!(packed.read(i), expected[i]);
    }
  }

  #[test]
  fn it_can_write_packed_values_with_5_bits_per_value() {
    let bit_size = 5;
    let original = vec![1, 31, 18, 30, 26, 5, 18, 31, 0];

    let mut packed = PackedLongArray::from_bytes(&original, bit_size);
    packed.write(2, 29);

    assert_eq!(packed.read(1), 31);
    assert_eq!(packed.read(2), 29);
    assert_eq!(packed.read(3), 30);
  }

  #[test]
  fn it_can_write_packed_values_with_multi_value_op() {
    // Here the change happens at index 10 which corresponds to bits 60..66
    // We need to make sure that the values on both sides should change
    // Originally data[10] = 001010  (10)
    //                 new = 110101  (48)
    let bit_size = 6;
    let original = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    let change = (10, 48);
    let expected = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 48, 11, 12];

    let mut packed = PackedLongArray::from_bytes(&original, bit_size);
    packed.write(change.0, change.1);
    for i in 0..expected.len() {
      assert_eq!(packed.read(i), expected[i]);
    }
  }

  #[test]
  fn it_can_write_packed_values_with_8_bits_per_value() {
    let bit_size = 8;
    let original = vec![1, 31, 18, 30, 26, 5, 18, 31, 0];
    let changes = vec![(0, 13), (2, 27), (3, 0), (8, 18)];
    let expected = vec![13, 31, 27, 0, 26, 5, 18, 31, 18];

    let mut packed = PackedLongArray::from_bytes(&original, bit_size);
    for (i, value) in changes.iter() {
      packed.write(*i, *value);
    }

    for i in 0..expected.len() {
      assert_eq!(packed.read(i), expected[i]);
    }
  }

  fn search_vec(bytes: &[u8], search: u8) -> Option<usize> {
    for (i, byte) in bytes.iter().enumerate() {
      if *byte == search {
        return Some(i);
      }
    }
    None
  }

  #[test]
  fn it_can_search_packed_values_with_8_bits_per_value() {
    let bit_size = 8;
    let original = vec![255, 128, 127, 64, 63, 31, 15, 7, 3];

    let packed = PackedLongArray::from_bytes(&original, bit_size);
    for search in 1..=255 {
      let expected = search_vec(&original, search);
      let actual = packed.search(search);
      assert_eq!(actual, expected, "Searching for {}", search);
    }
  }

  #[test]
  fn it_can_search_packed_values_with_4_bits_per_value() {
    let bit_size = 4;
    let original = vec![15, 13, 11, 9, 7, 5, 3, 1, 2];

    let packed = PackedLongArray::from_bytes(&original, bit_size);
    for search in 1..=255 {
      let expected = search_vec(&original, search);
      let actual = packed.search(search);
      assert_eq!(actual, expected, "Searching for {}", search);
    }
  }

  #[test]
  fn it_can_search_packed_values_with_5_bits_per_value() {
    let bit_size = 5;
    let original = vec![31, 18, 15, 8, 0, 1, 2];

    let packed = PackedLongArray::from_bytes(&original, bit_size);
    for search in 1..=255 {
      let expected = search_vec(&original, search);
      let actual = packed.search(search);
      assert_eq!(actual, expected, "Searching for {}", search);
    }
  }

  #[test]
  fn it_can_search_and_handle_padded_zeros() {
    let bit_size = 4;
    let original = vec![15, 13, 11, 9, 7, 5, 3, 1, 2];

    let packed = PackedLongArray::from_bytes(&original, bit_size);
    assert_eq!(packed.search(0), None);
  }
}



