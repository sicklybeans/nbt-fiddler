use std::collections::HashMap;
use std::convert::TryInto;
use std::fs;
use std::ops::Index;
use std::string::FromUtf8Error;

use crate::util;
use crate::util::truncated_list_str;

#[derive(Clone)]
pub enum Tag {
  Byte(u8),
  Short(i16),
  Int(i32),
  Long(i64),
  Float(f32),
  Double(f64),
  ByteArray(Vec<u8>),
  String(String),
  List(Vec<Tag>, u8),
  Compound(CompoundData),
  IntArray(Vec<i32>),
  LongArray(Vec<i64>),
}

pub type NBTFile = Tag;

pub struct NBTTagIterator<'a> {
  it: Option<std::collections::hash_map::Iter<'a, String, Tag>>
}

#[derive(Debug)]
pub enum NBTError {
  BadUtf8(FromUtf8Error),
  ReadError(std::io::Error),
  UnknownTagId(u8),
  UnexpectedTag(String),
  DecompressionError(std::io::Error),
  UnknownError(String),
}

#[derive(Clone)]
pub struct CompoundData {
  names: Vec<String>,
  mapped: HashMap<String, usize>,
  tags: Vec<Option<Tag>>,
}

impl From<NBTError> for String {
  fn from(e: NBTError) -> Self {
    use NBTError::*;
    match e {
      BadUtf8(e) => format!("Bad UTF string: {:?}", e),
      ReadError(e) => format!("Error reading file: {:?}", e),
      UnknownTagId(e) => format!("Tag had invalid type id: {}", e),
      UnexpectedTag(msg) => format!("Unexpected tag: {}", msg),
      DecompressionError(e) => format!("Could not decompress NBT object: {:?}", e),
      UnknownError(e) => format!("Unknown error: {:?}", e),
    }
  }
}

pub struct AsTypeError(u8, u8);

impl Tag {

  pub fn read(fname: &str) -> Result<Self, NBTError> {
    let bytes = fs::read(fname)
      .map_err(|e| NBTError::ReadError(e))?;
    Self::from(&bytes)
  }

  pub fn from(bytes: &[u8]) -> Result<Self, NBTError> {
    use NBTError::*;

    let (result, size) = Self::parse_tag(&bytes)?;

    if size != bytes.len() {
      return Err(UnexpectedTag(format!(
        "Parsing root NBT tag did not consume full buffer (read = {}, total = {})",
        size,
        bytes.len())));
    }

    match result {
      None => Err(NBTError::UnexpectedTag(format!("Root tag was an end tag!"))),
      Some((name, root_tag)) => {
        if name.len() != 0 {
          eprintln!("Warning: Found non-empty named tag at root of structure (name = {})", name);
          eprintln!("         This will be discarded in final object");
        }

        // Check that length of resulting structure agrees with input bytes
        let computed_length = 1 + 2 + name.as_bytes().len() + root_tag.byte_size();
        if computed_length != bytes.len() {
          // Only for testing purposes. If code is bug free this should never happen
          return Err(NBTError::UnexpectedTag(format!(
            "Internal Bug: Post parse byte calculation was off! (read = {} != computed = {})",
            root_tag.byte_size() + 3 + name.as_bytes().len(),
            bytes.len())));
        }

        // Check that root structure is a compound tag
        if let Tag::Compound(_) = root_tag {
          Ok(root_tag)
        } else {
          Err(NBTError::UnexpectedTag("Root NBT tag was not a compound tag!".to_string()))
        }
      }
    }
  }

  pub fn id(&self) -> u8 {
    use Tag::*;
    match self {
      Byte(_) => 1,
      Short(_) => 2,
      Int(_) => 3,
      Long(_) => 4,
      Float(_) => 5,
      Double(_) => 6,
      ByteArray(_) => 7,
      String(_) => 8,
      List(_, _) => 9,
      Compound(_) => 10,
      IntArray(_) => 11,
      LongArray(_) => 12,
    }
  }

  pub fn label(&self) -> &'static str {
    Self::id_label(self.id())
  }

  pub fn id_label(tag_id: u8) -> &'static str {
    match tag_id {
      0 => "end",
      1 => "byte",
      2 => "short",
      3 => "int",
      4 => "long",
      5 => "float",
      6 => "double",
      7 => "byte array",
      8 => "string",
      9 => "list",
      10 => "compound",
      11 => "int array",
      12 => "long array",
      _ => panic!("Unknown tag id: {}", tag_id)
    }
  }

  pub fn byte_size(&self) -> usize {
    use Tag::*;
    match self {
      Byte(_) => 1,
      Short(_) => 2,
      Int(_) => 4,
      Long(_) => 8,
      Float(_) => 4,
      Double(_) => 8,
      ByteArray(items) => 4 + items.len(),
      String(v) => 2 + v.as_bytes().len(),
      List(items, _) => 5 + items.iter().map(|i| i.byte_size()).sum::<usize>(),
      IntArray(v) => 4 + v.len() * 4,
      LongArray(v) => 4 + v.len() * 8,
      Compound(items) => {
        1 + items.entries().iter().map(|(name, value)| {
          1 + 2 + name.as_bytes().len() + value.byte_size()
        }).sum::<usize>()
      }
    }
  }

  pub fn as_byte(&self) -> Result<u8, AsTypeError> {
    if let Tag::Byte(v) = self {
      Ok(*v)
    } else {
      Err(AsTypeError(1, self.id()))
    }
  }

  pub fn as_short(&self) -> Result<i16, AsTypeError> {
    if let Tag::Short(v) = self {
      Ok(*v)
    } else {
      Err(AsTypeError(2, self.id()))
    }
  }

  pub fn as_int(&self) -> Result<i32, AsTypeError> {
    if let Tag::Int(v) = self {
      Ok(*v)
    } else {
      Err(AsTypeError(3, self.id()))
    }
  }

  pub fn as_long(&self) -> Result<i64, AsTypeError> {
    if let Tag::Long(v) = self {
      Ok(*v)
    } else {
      Err(AsTypeError(4, self.id()))
    }
  }

  pub fn as_byte_array(&self) -> Result<&[u8], AsTypeError> {
    if let Tag::ByteArray(v) = self {
      Ok(v)
    } else {
      Err(AsTypeError(7, self.id()))
    }
  }

  pub fn as_str(&self) -> Result<&str, AsTypeError> {
    if let Tag::String(s) = self {
      Ok(s)
    } else {
      Err(AsTypeError(8, self.id()))
    }
  }

  pub fn into_string(self) -> Result<String, AsTypeError> {
    if let Tag::String(s) = self {
      Ok(s)
    } else {
      Err(AsTypeError(8, self.id()))
    }
  }

  pub fn as_list(&self) -> Result<&Vec<Tag>, AsTypeError> {
    if let Tag::List(items, _) = self {
      Ok(items)
    } else {
      Err(AsTypeError(9, self.id()))
    }
  }

  pub fn as_list_mut(&mut self) -> Result<&mut Vec<Tag>, AsTypeError> {
    if let Tag::List(items, _) = self {
      Ok(items)
    } else {
      Err(AsTypeError(9, self.id()))
    }
  }

  pub fn into_list(self) -> Result<Vec<Tag>, AsTypeError> {
    if let Tag::List(items, _) = self {
      Ok(items)
    } else {
      Err(AsTypeError(9, self.id()))
    }
  }

  pub fn as_compound(&self) -> Result<&CompoundData, AsTypeError> {
    if let Tag::Compound(data) = self {
      Ok(data)
    } else {
      Err(AsTypeError(10, self.id()))
    }
  }

  pub fn as_compound_mut(&mut self) -> Result<&mut CompoundData, AsTypeError> {
    if let Tag::Compound(data) = self {
      Ok(data)
    } else {
      Err(AsTypeError(10, self.id()))
    }
  }

  pub fn as_int_array(&self) -> Result<&[i32], AsTypeError> {
    if let Tag::IntArray(v) = self {
      Ok(v)
    } else {
      Err(AsTypeError(11, self.id()))
    }
  }

  pub fn as_long_array(&self) -> Result<&[i64], AsTypeError> {
    if let Tag::LongArray(v) = self {
      Ok(v)
    } else {
      Err(AsTypeError(12, self.id()))
    }
  }

  pub fn into_long_array(self) -> Result<Vec<i64>, AsTypeError> {
    if let Tag::LongArray(v) = self {
      Ok(v)
    } else {
      Err(AsTypeError(12, self.id()))
    }
  }

  fn parse_tag_name(buf: &[u8]) -> Result<(String, usize), NBTError> {
    let length = u16::from_be_bytes([buf[0], buf[1]]) as usize;
    let name = String::from_utf8(buf[2..(2 + length)].to_vec())?;
    Ok((name, 2 + length))
  }

  fn parse_tag(buf: &[u8]) -> Result<(Option<(String, Self)>, usize), NBTError> {
    let tag_id = buf[0];
    let mut offset = 1;

    if tag_id == 0 {
      return Ok((None, 1));
    } else {

      let (name, name_size) = Self::parse_tag_name(&buf[offset..])?;
      offset += name_size;

      let (payload, payload_offset) = Tag::parse_payload(tag_id, &buf[offset..])?;
      offset += payload_offset;

      Ok((Some((name, payload)), offset))
    }
  }

  fn parse_payload(tag_id: u8, payload_buffer: &[u8]) -> Result<(Self, usize), NBTError> {
    let b = payload_buffer;
    let (p, size) =
    match tag_id {
      1 => Ok((Self::Byte(b[0]), 1)),
      2 => Ok((Self::Short(i16::from_be_bytes([b[0], b[1]])), 2)),
      3 => Ok((Self::Int(i32::from_be_bytes(b[0..4].try_into().unwrap())), 4)),
      4 => Ok((Self::Long(i64::from_be_bytes(b[0..8].try_into().unwrap())), 8)),
      5 => Ok((Self::Float(f32::from_be_bytes(b[0..4].try_into().unwrap())), 4)),
      6 => Ok((Self::Double(f64::from_be_bytes(b[0..8].try_into().unwrap())), 8)),
      7 => Ok(Self::parse_byte_array(b)),
      8 => Ok(Self::parse_string(b)?),
      9 => Ok(Self::parse_list(b)?),
      10 => Ok(Self::parse_compound(b)?),
      11 => Ok(Self::parse_int_array(b)),
      12 => Ok(Self::parse_long_array(b)),
      _ => Err(NBTError::UnknownTagId(tag_id)),
    }?;
    Ok((p, size))
  }

  fn parse_byte_array(buf: &[u8]) -> (Self, usize) {
    let length = i32::from_be_bytes(buf[0..4].try_into().unwrap()) as usize;
    let bytes = buf[4..(4 + length)].to_vec();
    (Self::ByteArray(bytes), 4 + length)
  }

  fn parse_int_array(buf: &[u8]) -> (Self, usize) {
    let length = i32::from_be_bytes(buf[0..4].try_into().unwrap()) as usize;
    let mut values = vec![0; length];
    let mut offset = 4;
    for i in 0..length {
      values[i] = i32::from_be_bytes(buf[offset..(offset + 4)].try_into().unwrap());
      offset += 4;
    }
    (Self::IntArray(values), offset)
  }

  fn parse_long_array(buf: &[u8]) -> (Self, usize) {
    let length = i32::from_be_bytes(buf[0..4].try_into().unwrap()) as usize;
    let mut values = vec![0; length];
    let mut offset = 4;
    for i in 0..length {
      values[i] = i64::from_be_bytes(buf[offset..(offset + 8)].try_into().unwrap());
      offset += 8;
    }
    (Self::LongArray(values), offset)
  }

  fn parse_string(buf: &[u8]) -> Result<(Self, usize), NBTError> {
    let length = u16::from_be_bytes([buf[0], buf[1]]) as usize;
    String::from_utf8(buf[2..(2 + length)].to_vec())
      .map_err(|e| NBTError::BadUtf8(e))
      .map(|value| (Self::String(value), 2 + length))
  }

  fn parse_list(buf: &[u8]) -> Result<(Self, usize), NBTError> {
    let tag_id = buf[0];
    let length = i32::from_be_bytes(buf[1..5].try_into().unwrap()) as usize;
    let mut offset = 5;
    let mut payloads = Vec::new();
    for _ in 0..length {
      let (payload, payload_size) = Self::parse_payload(tag_id, &buf[offset..])?;
      payloads.push(payload);
      offset += payload_size;
    }
    Ok((Self::List(payloads, tag_id), offset))
  }

  fn parse_compound(buf: &[u8]) -> Result<(Self, usize), NBTError> {
    let mut offset = 0;
    let mut mapped = HashMap::new();
    let mut names = Vec::new();
    let mut tags = Vec::new();
    // let mut tags = HashMap::new();
    loop {
      let (parsed, tag_size) = Self::parse_tag(&buf[offset..])?;
      offset += tag_size;
      if let Some((name, tag)) = parsed {
        mapped.insert(name.clone(), tags.len());
        names.push(name);
        tags.push(Some(tag));
        // tags.insert(name, tag);
      } else {
        break;
      }
    }
    Ok((Self::Compound(CompoundData { mapped, tags, names }), offset))
  }

  fn fmt_helper(&self, f: &mut std::fmt::Formatter, name: &str, indent: u8) -> std::fmt::Result {
    let indent_str = util::indent_str(indent);
    if let Tag::Compound(data) = self {
      write!(f, "{}{} = {{\n", indent_str, name)?;
      for (child_name, child_tag) in data.entries().iter() {
        child_tag.fmt_helper(f, child_name, indent + 1)?;
        write!(f, "\n")?;
      }
      write!(f, "{}}}", indent_str)
    } else {
      write!(f, "{}{} = {}", indent_str, name, self)
    }
  }

  pub fn deep_equality(&self, other: &Self) -> bool {
    fn cmp_arr<T: std::cmp::PartialEq>(arr1: &[T], arr2: &[T]) -> bool {
      if arr1.len() != arr2.len() {
        false
      } else {
        for i in 0..arr1.len() {
          if arr1[i] != arr2[i] {
            return false;
          }
        }
        true
      }
    }

    use Tag::*;
    match (self, other) {
      (Byte(v1), Byte(v2)) => v1 == v2,
      (Short(v1), Short(v2)) => v1 == v2,
      (Int(v1), Int(v2)) => v1 == v2,
      (Long(v1), Long(v2)) => v1 == v2,
      (Float(v1), Float(v2)) => v1 == v2,
      (Double(v1), Double(v2)) => v1 == v2,
      (String(v1), String(v2)) => v1 == v2,
      (ByteArray(arr1), ByteArray(arr2)) => cmp_arr(arr1, arr2),
      (IntArray(arr1), IntArray(arr2)) => cmp_arr(arr1, arr2),
      (LongArray(arr1), LongArray(arr2)) => cmp_arr(arr1, arr2),
      (List(arr1, _), List(arr2, _)) => {
        if arr1.len() != arr2.len() {
          return false;
        }
        for i in 0..arr1.len() {
          if !arr1[i].deep_equality(&arr2[i]) {
            return false;
          }
        }
        true
      },
      (Compound(map1), Compound(map2)) => {
        if map1.tags.len() != map2.tags.len() {
          return false;
        }
        for i in 0..map1.tags.len() {
          if map1.names[i] != map2.names[i] {
            return false;
          }
          match (map1.tags[i].as_ref(), map2.tags[i].as_ref()) {
            (Some(tag1), Some(tag2)) => {
              if !tag1.deep_equality(&tag2) {
                return false;
              }
            },
            (None, None) => {},
            _ => {
              return false;
            }
          }
        }
        true
      }
      _ => false,
    }
  }

}

impl std::fmt::Debug for Tag {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    match self {
      Tag::Byte(b) => write!(f, "{} (byte)", b),
      Tag::Short(v) => write!(f, "{} (short)", v),
      Tag::Int(v) => write!(f, "{} (int)", v),
      Tag::Long(v) => write!(f, "{} (long)", v),
      Tag::Float(v) => write!(f, "{} (float)", v),
      Tag::Double(v) => write!(f, "{} (double)", v),
      Tag::String(v) => write!(f, "{} (string)", v),
      Tag::ByteArray(v) => write!(f, "{} ({})", truncated_list_str(&v, 3), self.label()),
      Tag::IntArray(v) => write!(f, "{} ({})", truncated_list_str(&v, 3), self.label()),
      Tag::LongArray(v) => write!(f, "{} ({})", truncated_list_str(&v, 3), self.label()),
      Tag::List(v, tag_id) => write!(f, "[(size = {})] ({})", v.len(), Self::id_label(*tag_id)),
      Tag::Compound(_) => write!(f, "{{ compound }}"),
    }
  }
}

impl std::fmt::Display for Tag {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    match self {
      Tag::Byte(b) => write!(f, "{} (byte)", b),
      Tag::Short(v) => write!(f, "{} (short)", v),
      Tag::Int(v) => write!(f, "{} (int)", v),
      Tag::Long(v) => write!(f, "{} (long)", v),
      Tag::Float(v) => write!(f, "{} (float)", v),
      Tag::Double(v) => write!(f, "{} (double)", v),
      Tag::String(v) => write!(f, "{} (string)", v),
      Tag::ByteArray(v) => write!(f, "{} ({})", truncated_list_str(&v, 3), self.label()),
      Tag::IntArray(v) => write!(f, "{} ({})", truncated_list_str(&v, 3), self.label()),
      Tag::LongArray(v) => write!(f, "{} ({})", truncated_list_str(&v, 3), self.label()),
      Tag::List(v, tag_id) => write!(f, "[(size = {})] ({})", v.len(), Self::id_label(*tag_id)),
      Tag::Compound(_) => self.fmt_helper(f, "", 0),
    }
  }
}

impl Index<&str> for Tag {
  type Output = Tag;

  fn index(&self, key: &str) -> &<Self as Index<&str>>::Output {
    if let Tag::Compound(ref data) = self {
      let index = &data.mapped[key];
      &data.tags[*index].as_ref().unwrap()
    } else {
      panic!("Cannot index a non-compound tag!")
    }
  }
}

impl<'a> Iterator for NBTTagIterator<'a> {
  type Item = (&'a String, &'a Tag);

  fn next(&mut self) -> Option<<Self as Iterator>::Item> {
    if let Some(it) = &mut self.it {
      it.next()
    } else {
      None
    }
  }
}

impl From<FromUtf8Error> for NBTError {
  fn from(e: FromUtf8Error) -> Self {
    NBTError::BadUtf8(e)
  }
}

impl std::fmt::Display for AsTypeError {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "{}", String::from(self))
  }
}

impl std::fmt::Debug for AsTypeError {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "{}", String::from(self))
  }
}

impl From<&AsTypeError> for String {
  fn from(e: &AsTypeError) -> String {
    format!("Expected tag of type {} but found {}", Tag::id_label(e.0), Tag::id_label(e.1))
  }
}

impl CompoundData {

  pub fn print(&self) {
    println!("Exploded compound view");
    println!("  keys: {:?}", self.mapped.iter().map(|(name, _)| name.clone()).collect::<Vec<String>>());
    println!("  values:");
    for (name, index) in self.mapped.iter() {
      println!("  {} = {:?}", name, self.tags[*index]);
    }

  }

  pub fn entries(&self) -> Vec<(&str, &Tag)> {
    self.tags.iter()
      .enumerate()
      .filter_map(|(i, tag)| if let Some(t) = tag {
        Some((self.names[i].as_str(), t))
      } else {
        None
      })
      .collect()
  }

  pub fn get(&self, key: &str) -> Option<&Tag> {
    if let Some(index) = self.mapped.get(key) {
      self.tags[*index].as_ref()
    } else {
      None
    }
  }

  pub fn get_mut(&mut self, key: &str) -> Option<&mut Tag> {
    if let Some(index) = self.mapped.get(key) {
      self.tags[*index].as_mut()
    } else {
      None
    }
  }

  pub fn pop(&mut self, key: &str) -> Option<Tag> {
    if let Some(index) = self.mapped.get(key) {
      self.tags[*index].take()
    } else {
      None
    }
  }

  pub fn insert(&mut self, key: &str, tag: Tag) -> Result<(), String> {
    if let Some(index) = self.mapped.get(key) {
      self.tags[*index] = Some(tag);
      Ok(())
    } else {
      Err(format!("Cannot insert unknown key: {}", key))
    }
  }

}

struct BufferWriter {
  pos: usize,
  size: usize,
  buf: Vec<u8>,
}

impl BufferWriter {

  fn new(size: usize) -> BufferWriter {
    BufferWriter { pos: 0, size, buf: vec![0; size] }
  }

  fn write(&mut self, bytes: &[u8]) -> usize {
    if bytes.len() > (self.buf.len() - self.pos) {
      panic!("TagWriter out of buffer room. Fix your code idiot");
    }
    for i in 0..bytes.len() {
      self.buf[self.pos + i] = bytes[i];
    }
    self.pos += bytes.len();
    bytes.len()
  }

  fn into_buffer(self) -> Vec<u8> {
    if self.buf.len() != self.size {
      panic!("Cannot finalize writer, the buffer size did not match the written size (num written = {}, num expected = {})", self.pos, self.size);
    }
    self.buf
  }
}

pub struct TagWriter {}

impl TagWriter {

  pub fn write(tag: &Tag) -> Vec<u8> {
    let mut bw = BufferWriter::new(3 + tag.byte_size());
    bw.write(&[tag.id()]); // top level tag id
    bw.write(&[0, 0]); // name length (empty)
    Self::write_payload(&mut bw, tag);
    bw.into_buffer()
  }

  fn write_payload(bw: &mut BufferWriter, tag: &Tag) -> usize {
    let mut count = 0;
    let written = match tag {
      Tag::Byte(b) => bw.write(&[*b]),
      Tag::Short(b) => bw.write(&b.to_be_bytes()),
      Tag::Int(b) => bw.write(&b.to_be_bytes()),
      Tag::Long(b) => bw.write(&b.to_be_bytes()),
      Tag::Float(b) => bw.write(&b.to_be_bytes()),
      Tag::Double(b) => bw.write(&b.to_be_bytes()),
      Tag::ByteArray(bytes) => {
        let length: i32 = bytes.len() as i32;
        count += bw.write(&length.to_be_bytes());
        count += bw.write(&bytes);
        count
      },
      Tag::IntArray(values) => {
        let length: i32 = values.len() as i32;
        count += bw.write(&length.to_be_bytes());
        for value in values.iter() {
          count += bw.write(&value.to_be_bytes());
        }
        count
      },
      Tag::LongArray(values) => {
        let length: i32 = values.len() as i32;
        count += bw.write(&length.to_be_bytes());
        for value in values.iter() {
          count += bw.write(&value.to_be_bytes());
        }
        count
      },
      Tag::String(s) => {
        let bytes = s.as_bytes();
        let length: u16 = bytes.len() as u16;
        count += bw.write(&length.to_be_bytes());
        count += bw.write(bytes);
        count
      },
      Tag::List(items, tag_id) => {
        let length: i32 = items.len() as i32;
        count += bw.write(&[*tag_id]);
        count += bw.write(&length.to_be_bytes());
        for item in items.iter() {
          count += Self::write_payload(bw, item);
        }
        count
      },
      Tag::Compound(map) => {
        for (name, item) in map.entries().iter() {
          let id = item.id();
          let name_bytes = name.as_bytes();
          let name_length: u16 = name_bytes.len() as u16;
          count += bw.write(&[id]);
          count += bw.write(&name_length.to_be_bytes());
          count += bw.write(&name_bytes);
          count += Self::write_payload(bw, item);
        }
        count += bw.write(&[0]);
        count
        // let length: i32 = items.len() as i32;
        // bw.write(&[*tag_id]);
        // bw.write(&length.to_be_bytes());
        // for item in items.iter() {
        //   Self::write_payload(bw, item);
        // }
        // let mut written = 0;
        // for (name, item) in map.iter() {
        //   let id = item.id();
        //   let name_length: u16 = name.as_bytes().len() as u16;
        //   written += Self::write_bytes_1(&mut buf[written..], id);
        //   written += Self::write_bytes_2(&mut buf[written..], name_length.to_be_bytes());
        //   written += Self::write_bytes_n(&mut buf[written..], name.as_bytes());
        //   written += Self::write_payload(&mut buf[written..], item);
        // }
        // written += Self::write_bytes_1(&mut buf[written..], 0);
        // written
      },
    };
    if written != tag.byte_size() {
      panic!("Byte count mismatch! Wrote {} but expected {} ({})", written, tag.byte_size(), tag);
    }
    written
  }

}
