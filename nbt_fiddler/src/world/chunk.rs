use std::fmt::{Debug, Display};
use std::collections::{HashSet};
use std::convert::TryFrom;

use crate::nbt::Tag;
use super::{
  PosNotInContainerError,
  SpatiallyBounded,
  section::{
    Section,
    SectionCoordinates,
    SetBlockError as SectionSetBlockError,
    GetBlockError as SectionGetBlockError,
  },
};

pub struct Chunk {
  pub chunk_x: u8,
  pub chunk_z: u8,
  pub level: Level,
  pub chunk_nbt: Tag,
  modified: bool,
}

pub struct Level {
  pub sections: Vec<Section>,
  pub x_pos: i32,
  pub z_pos: i32,
  pub level_nbt: Tag,
}

#[derive(Debug)]
pub enum BlockError<T> where T: Display + Debug {
  BlockNotInChunkError(PosNotInContainerError),
  SectionError(T),
  SectionMissingError,
}

pub type GetBlockError = BlockError<SectionGetBlockError>;

pub type SetBlockError = BlockError<SectionSetBlockError>;

impl Chunk {

  pub fn new(chunk_nbt: Tag, level: Level, chunk_x: u8, chunk_z: u8) -> Chunk {
    Chunk { chunk_x, chunk_z, level, chunk_nbt, modified: false, }
  }

  pub fn is_modified(&self) -> bool {
    return self.modified;
  }

  fn get_section_containing(&self, y: u8) -> Option<&Section> {
    let section_index = (y / 16) as u8 + 1; // first section is always light layer or something
    if section_index < (self.level.sections.len() as u8) {
      Some(&self.level.sections[section_index as usize])
    } else {
      None
    }
  }

  fn get_section_containing_mut(&mut self, y: u8) -> Option<&mut Section> {
    let section_index = (y / 16) as u8 + 1; // first section is always light layer or something
    if section_index < (self.level.sections.len() as u8) {
      Some(&mut self.level.sections[section_index as usize])
    } else {
      None
    }
  }

  fn get_real_coordinates(&self, section: &Section, relative: &SectionCoordinates) -> (i32, u8, i32) {
    let lower = self.get_lower_bounds();
    (lower.0 + relative.x as i32, 16 * section.y + relative.y, lower.2 + relative.z as i32)
  }

  fn get_section_coordinates(&self, x: i32, y: u8, z: i32) -> Result<SectionCoordinates, PosNotInContainerError> {
    let lower = self.get_lower_bounds();
    let section_x = u8::try_from(x - lower.0)
      .map_err(|_| PosNotInContainerError::new((x, y, z), self))?;
    let section_y = y % 16;
    let section_z = u8::try_from(z - lower.2)
      .map_err(|_| PosNotInContainerError::new((x, y, z), self))?;
    Ok(SectionCoordinates::new(section_x, section_y, section_z))
  }

  pub fn get_block_at(&self, x: i32, y: u8, z: i32) -> Result<Option<String>, GetBlockError> {
    let coordinates = self.get_section_coordinates(x, y, z)
      .map_err(|e| BlockError::BlockNotInChunkError(e))?;
    let section = self.get_section_containing(y)
      .ok_or(BlockError::SectionMissingError)?;
    // // double check y is correct
    // assert_eq!(y >= (section.y * 16), true);
    // assert_eq!((y as usize) < ((section.y as usize + 1) * 16), true);
    section.get_block_at(coordinates)
      .map_err(|e| BlockError::SectionError(e))
  }

  pub fn set_block_at(&mut self, x: i32, y: u8, z: i32, name: &str) -> Result<(), SetBlockError> {
    let coordinates = self.get_section_coordinates(x, y, z)
      .map_err(|e| BlockError::BlockNotInChunkError(e))?;
    let section = self.get_section_containing_mut(y)
      .ok_or(BlockError::SectionMissingError)?;
    section.set_block_at(coordinates, name)
      .map_err(|e| BlockError::SectionError(e))?;
    self.modified = true;
    Ok(())
  }

  pub fn print(&self) {
    println!("Chunk relative position: {}, {}", self.chunk_x, self.chunk_z);
    println!("Chunk position         : {}, {}", self.level.x_pos, self.level.z_pos);
  }

  pub fn get_palette(&self) -> HashSet<String> {
    self.level.sections.iter().map(|s| s.get_palette()).flatten().collect()
  }

  pub fn contains_block(&self, name: &str) -> bool {
    for section in self.level.sections.iter() {
      if section.contains_block(name) {
        return true;
      }
    }
    return false;
  }

  pub fn search_block_names(&self, name: &str) -> Vec<&Section> {
    self.level.sections.iter()
      .filter_map(|section| {
        if section.contains_block(name) {
          Some(section)
        } else {
          None
        }
      })
      .collect()
  }

  /// Looks for blocks whose states don't appear in the Palette for their section.
  ///
  /// This is basically the driving reason why I created this library. A few mods corrupted my
  /// beautiful world by setting some block states to invalid values. I am hoping that removing
  /// these blocks will fix the problem.
  pub fn print_corrupt_blocks(&self) {
    for section in self.level.sections.iter() {
      let blocks = section.find_corrupt_blocks();
      if blocks.len() > 0 {
        println!("Found {} corrupt blocks in section y={}", blocks.len(), section.y);
        println!("Section palette:");
        for (i, p) in section.palette.as_ref().unwrap().iter().enumerate() {
          println!("  [{}] = {}", i, p.name);
        }
        println!("Corrupted blocks:");
        for (pos, val) in blocks.iter() {
          println!("  {:?} = {}", pos, val);
        }
      }
    }
  }

  /// Looks for blocks whose states don't appear in the Palette for their section.
  ///
  /// This is basically the driving reason why I created this library. A few mods corrupted my
  /// beautiful world by setting some block states to invalid values. I am hoping that removing
  /// these blocks will fix the problem.
  pub fn find_corrupt_blocks(&self) -> Vec<((i32, u8, i32), u8)> {
    self.level.sections.iter()
      .map(|section| {
        section.find_corrupt_blocks()
          .iter()
          .map(|(relative, state)| (self.get_real_coordinates(section, relative), *state))
          .collect::<Vec<((i32, u8, i32), u8)>>()
      })
      .flatten()
      .collect()
  }

  /// Looks for blocks whose states don't appear in the Palette for their section.
  ///
  /// This is basically the driving reason why I created this library. A few mods corrupted my
  /// beautiful world by setting some block states to invalid values. I am hoping that removing
  /// these blocks will fix the problem.
  pub fn fix_corrupt_blocks(&mut self, with: &str) {
    for section in self.level.sections.iter_mut() {
      section.fix_corrupt_blocks(with);
    }
    self.modified = true;
  }

  pub fn remove_ticks(&mut self) {
    let level_nbt = &mut self.level.level_nbt;
    let ticks = level_nbt.as_compound_mut().unwrap().get_mut("TileTicks").unwrap().as_list_mut().unwrap();
    while ticks.len() > 0 {
      ticks.pop();
    }
    self.modified = true;
  }

  pub fn remove_tile_entities(&mut self, id: &str) {
    let level_nbt = &mut self.level.level_nbt;
    let tiles = level_nbt.as_compound_mut().unwrap().get_mut("TileEntities").unwrap().as_list_mut().unwrap();
    let mut keep_tiles = Vec::new();
    while tiles.len() > 0 {
      let tile = tiles.pop().unwrap();
      if tile.as_compound().unwrap().get("id").unwrap().as_str().unwrap() == id {
        keep_tiles.push(tile);
      }
    }
    while keep_tiles.len() > 0 {
      tiles.push(keep_tiles.pop().unwrap());
    }
    self.modified = true;
  }

  pub fn replace_blocks(&mut self, find: &str, replace: &str) {
    for section in self.level.sections.iter_mut() {
      section.replace_blocks(find, replace);
    }
    self.modified = true;
  }
}

impl Level {

  pub fn new(level_nbt: Tag, sections: Vec<Section>, x_pos: i32, z_pos: i32) -> Level {
    Level { level_nbt, sections, x_pos, z_pos }
  }

}

impl std::fmt::Display for Chunk {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "Chunk ({}, {})", self.chunk_x, self.chunk_z)
  }
}

impl SpatiallyBounded for Chunk {

  fn get_lower_bounds(&self) -> (i32, u8, i32) {
    let x = (self.level.x_pos) * 16;
    let z = (self.level.z_pos) * 16;
    let y = 0;
    (x, y, z)
  }

  fn get_upper_bounds(&self) -> (i32, u8, i32) {
    let x = (self.level.x_pos + 1) * 16;
    let z = (self.level.z_pos + 1) * 16;
    let y = 255;
    (x, y, z)
  }

}

impl<T> Display for BlockError<T> where T: Display + Debug {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    match self {
      BlockError::SectionMissingError => write!(f, "TODO: Section not stored in chunk"),
      BlockError::SectionError(e) => write!(f, "{}", e),
      BlockError::BlockNotInChunkError(e) => write!(f, "{}", e),
    }
  }
}
