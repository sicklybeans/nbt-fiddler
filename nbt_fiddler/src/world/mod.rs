mod world;
mod region;
mod section;
mod chunk;
mod loader;

pub struct PosNotInContainerError {
  msg: String
}

pub type Coordinate = (i32, u8, i32);

pub trait SpatiallyBounded: std::fmt::Display + Sized {

  fn get_upper_bounds(&self) -> (i32, u8, i32);

  fn get_lower_bounds(&self) -> (i32, u8, i32);

  fn check_contains_pos<'a>(&'a self, x: i32, y: u8, z: i32) -> Result<(), PosNotInContainerError> {
    if !self.contains_pos(x, y, z) {
      Err(PosNotInContainerError::new((x, y, z), self))
    } else {
      Ok(())
    }
  }

  fn contains_pos(&self, x: i32, y: u8, z: i32) -> bool {
    let b1 = self.get_lower_bounds();
    let b2 = self.get_upper_bounds();
    x >= b1.0 && x < b2.0 && y >= b1.1 && y < b2.1 && z >= b1.2 && z < b2.2
  }
}

impl PosNotInContainerError {
  pub fn new<T, U>(pos: T, container: &U) -> PosNotInContainerError
  where T: std::fmt::Debug,
        U: std::fmt::Display {
    PosNotInContainerError {
      msg: format!("Position {:?} is not located within {}", pos, container)
    }
  }
}

impl std::fmt::Display for PosNotInContainerError {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "{}", self.msg)
  }
}

impl std::fmt::Debug for PosNotInContainerError {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "{}", self.msg)
  }
}

impl From<PosNotInContainerError> for String {
  fn from(e: PosNotInContainerError) -> String {
    format!("{}", e)
  }
}

pub use world::World;
pub use region::Region;
pub use chunk::Chunk;
