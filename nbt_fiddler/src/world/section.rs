use std::convert::TryFrom;

use crate::nbt::Tag;
use crate::util;

pub struct Section {
  pub y: u8,
  pub block_states: Option<util::PackedLongArray>,
  pub palette: Option<Vec<Palette>>,
  pub section_nbt: Tag,
  modified: bool,
}

pub struct Palette {
  pub name: String,
  pub palette_nbt: Tag,
}

#[derive(Debug)]
pub struct SectionCoordinates {
  pub x: u8, pub y: u8, pub z: u8
}

pub enum GetBlockError {
  BlockNotInPalette
}

pub enum SetBlockError {
  SectionEmpty,
  PaletteTooLarge,
  BlockNotInPalette(String),
}

impl Section {

  pub fn new(
    section_nbt: Tag,
    y: u8,
    block_states: Option<util::PackedLongArray>,
    palette: Option<Vec<Palette>>,) -> Section {
    Section { section_nbt, y, block_states, palette, modified: false }
  }

  pub fn contains_block(&self, name: &str) -> bool {
    if let Some((palette, block_states)) = self.get_palette_states() {
      // Find state index in palette
      if let Ok(Some(state_index)) = Self::get_state_from_palette(palette, name) {
        // Make sure it actually appears in the array
        if let Some(_) = block_states.search(state_index) {
          return true;
        }
      }
    }
    return false;
  }

  pub fn get_palette(&self) -> Vec<String> {
    if let Some(palette) = &self.palette {
      palette.iter().map(|p| p.name.clone()).collect()
    } else {
      Vec::new()
    }
  }

  pub fn get_block_at(&self, pos: SectionCoordinates) -> Result<Option<String>, GetBlockError> {
    if let Some((palette, block_states)) = self.get_palette_states() {
      let state = block_states.read(pos.to_index()) as usize;
      if state >= palette.len() {
        Err(GetBlockError::BlockNotInPalette)
      } else {
        Ok(Some(palette[state].name.clone()))
      }
    } else {
      Ok(None)
    }
  }

  pub fn set_block_at(&mut self, pos: SectionCoordinates, name: &str) -> Result<(), SetBlockError> {
    if let Some((palette, block_states)) = self.get_palette_states_mut() {
      let state_result = Self::get_state_from_palette(palette, name)
        .map_err(|_| SetBlockError::PaletteTooLarge)?;
      if let Some(state) = state_result {
        block_states.write(pos.to_index(), state);
        self.modified = true;
        Ok(())
      } else {
        Err(SetBlockError::BlockNotInPalette(name.to_string()))
      }
    } else {
      Err(SetBlockError::SectionEmpty)
    }
  }

  /// Looks for blocks whose states don't appear in the Palette for their section.
  ///
  /// This is basically the driving reason why I created this library. A few mods corrupted my
  /// beautiful world by setting some block states to invalid values. I am hoping that removing
  /// these blocks will fix the problem.
  pub fn find_corrupt_blocks(&self) -> Vec<(SectionCoordinates, u8)> {
    if let Some((palette, block_states)) = self.get_palette_states() {
      let mut corrupted = Vec::new();
      for x in 0..16 {
        for y in 0..16 {
          for z in 0..16 {
            let pos = SectionCoordinates::new(x, y, z);
            let state = block_states.read(pos.to_index());
            if state as usize > palette.len() {
              corrupted.push((pos, state));
            }
          }
        }
      }
      corrupted
    } else {
      Vec::new()
    }
  }

  /// Looks for blocks whose states don't appear in the Palette for their section.
  ///
  /// This is basically the driving reason why I created this library. A few mods corrupted my
  /// beautiful world by setting some block states to invalid values. I am hoping that removing
  /// these blocks will fix the problem.
  pub fn fix_corrupt_blocks(&mut self, with: &str) {
    self.modified = true;
    if let Some((palette, block_states)) = self.get_palette_states_mut() {
      let replace_state = Self::get_state_from_palette(palette, with).unwrap().unwrap();
      for x in 0..16 {
        for y in 0..16 {
          for z in 0..16 {
            let pos = SectionCoordinates::new(x, y, z);
            let state = block_states.read(pos.to_index());
            if state as usize >= palette.len() {
              block_states.write(pos.to_index(), replace_state);
            }
          }
        }
      }
    }
  }

  pub fn replace_blocks(&mut self, find: &str, replace: &str) {
    self.modified = true;
    if self.contains_block(find) {
      if let Some((palette, block_states)) = self.get_palette_states_mut() {
        let find_state = Self::get_state_from_palette(palette, find).unwrap().unwrap();
        let replace_state = Self::get_state_from_palette(palette, replace).unwrap().unwrap();
        for x in 0..16 {
          for y in 0..16 {
            for z in 0..16 {
              let pos = SectionCoordinates::new(x, y, z);
              let state = block_states.read(pos.to_index());
              if state == find_state {
                block_states.write(pos.to_index(), replace_state);
              } else if state as usize >= palette.len() {
                block_states.write(pos.to_index(), replace_state);
              }
            }
          }
        }
      }
    }
  }

  fn get_state_from_palette(palette: &Vec<Palette>, name: &str) -> Result<Option<u8>, ()> {
    for (i, p) in palette.iter().enumerate() {
      if p.name == name {
        return u8::try_from(i)
          .map_err(|_| ())
          .map(|index| Some(index))
      }
    }
    Ok(None)
  }

  fn get_palette_states(&self) -> Option<(&Vec<Palette>, &util::PackedLongArray)> {
    if let (Some(palette), Some(packed)) = (&self.palette, &self.block_states) {
      Some((palette, packed))
    } else {
      None
    }
  }

  fn get_palette_states_mut(&mut self) -> Option<(&mut Vec<Palette>, &mut util::PackedLongArray)> {
    if let (Some(palette), Some(packed)) = (&mut self.palette, &mut self.block_states) {
      Some((palette, packed))
    } else {
      None
    }
  }
}

impl SectionCoordinates {

  pub fn new(x: u8, y: u8, z: u8) -> SectionCoordinates {
    SectionCoordinates { x, y, z }
  }

  fn to_index(&self) -> usize {
    ((self.y as usize) << 8) + ((self.z as usize) << 4) + self.x as usize
  }

}

impl Palette {

  pub fn new(palette_nbt: Tag, name: String) -> Palette {
    Palette { palette_nbt, name }
  }

}

impl std::fmt::Display for SetBlockError {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    match self {
      SetBlockError::PaletteTooLarge => {
        write!(f, "TODO: State was found in palette, but palette was larger than 256 which is currently not supported")
      },
      SetBlockError::BlockNotInPalette(name) => {
        write!(f, "TODO: Cannot set section block to {} because not in section palette", name)
      },
      SetBlockError::SectionEmpty => {
        write!(f, "TODO: Chunk section has no palette")
      }
    }
  }
}

impl std::fmt::Display for GetBlockError {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "Block state had a value that was not within the section palette (this is still a mystery)")
  }
}

impl std::fmt::Debug for GetBlockError {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "{}", self)
  }
}

impl std::fmt::Debug for SetBlockError {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "{}", self)
  }
}

impl From<SetBlockError> for String {
  fn from(e: SetBlockError) -> String {
    format!("{}", e)
  }
}
