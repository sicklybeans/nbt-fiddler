use flate2::{
  Compression,
  read::ZlibDecoder,
  write::ZlibEncoder,
};
use std::fs;
use std::io::{Read, Write};

const READ_BUFFER_SIZE: usize = 500000;

pub enum LoadCompressedFileError {
  FileReadError(std::io::Error),
  DecompressionError(std::io::Error),
}

pub fn load_compressed_file(fname: &str) -> Result<Vec<u8>, std::io::Error> {
  let bytes = fs::read(fname)?;
  let mut buffer = vec![0 as u8; READ_BUFFER_SIZE];
  ZlibDecoder::new(&bytes[0..]).read_to_end(&mut buffer)?;
  Ok(buffer)
}

pub fn compress(src: &[u8], dst: &mut [u8]) -> Result<usize, std::io::Error> {
  let mut encoder = ZlibEncoder::new(dst, Compression::best());
  encoder.write_all(src)?;
  encoder.flush()?;
  let size = encoder.total_out();
  encoder.finish()?;
  Ok(size as usize)
}
