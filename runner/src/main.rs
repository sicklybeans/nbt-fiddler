use nbt_fiddler::world::*;
use nbt_fiddler::nbt::Tag;

mod fix_modded_world;

// const WORLD_DIR: &'static str = "inputs/TestWorld_working";
// const REGION_X: i32 = 0;
// const REGION_Z: i32 = 0;
// const CHUNK_X: u8 = 0;
// const CHUNK_Z: u8 = 0;

const WORLD_DIR: &'static str = "inputs/TestWorld_working";
const REGION_X: i32 = 1;
const REGION_Z: i32 = -1;
const CHUNK_X: u8 = 14;
const CHUNK_Z: u8 = 18;

pub fn explore_user_structure() {
  let mut w = World::new(WORLD_DIR);
  let r = w.get_region(REGION_X, REGION_Z).unwrap();
  r.load_chunk(CHUNK_X, CHUNK_Z).unwrap();
  let chunk = &r.get_chunk(CHUNK_X, CHUNK_Z).unwrap();
  println!("Chunk{}", chunk.chunk_nbt);
  println!("Level{}", chunk.level.level_nbt);
  println!("TileEntity{}", chunk.level.level_nbt.as_compound().unwrap().get("TileEntities").unwrap().as_list().unwrap()[0]);
  println!("TileTicks{}", chunk.level.level_nbt.as_compound().unwrap().get("TileTicks").unwrap().as_list().unwrap()[0]);
  // for section in sections.iter() {
  //   println!("Section{}", section.section_nbt);
  //   if let Some(palette) = section.section_nbt.as_compound().unwrap().get("Palette") {
  //     for p in palette.as_list().unwrap() {
  //       println!("Palette{}", p);
  //     }
  //   }
  // }
}

pub fn explore_chunk_nbt_structure() {
  let mut w = World::new(WORLD_DIR);
  let r = w.get_region(REGION_X, REGION_Z).unwrap();
  r.load_chunk(CHUNK_X, CHUNK_Z).unwrap();
  let chunk = &r.get_chunk(CHUNK_X, CHUNK_Z).unwrap();
  println!("Chunk{}", chunk.chunk_nbt);
  println!("Level{}", chunk.level.level_nbt);
  println!("TileEntity{}", chunk.level.level_nbt.as_compound().unwrap().get("TileEntities").unwrap().as_list().unwrap()[0]);
  println!("TileTicks{}", chunk.level.level_nbt.as_compound().unwrap().get("TileTicks").unwrap().as_list().unwrap()[0]);
  // for section in sections.iter() {
  //   println!("Section{}", section.section_nbt);
  //   if let Some(palette) = section.section_nbt.as_compound().unwrap().get("Palette") {
  //     for p in palette.as_list().unwrap() {
  //       println!("Palette{}", p);
  //     }
  //   }
  // }
}

pub fn world_test() {
  let mut world = World::new(WORLD_DIR);
  let p = (0, 241, 0);
  println!("Block ({:?}) = {:?}", p, world.get_block_at(p.0, p.1, p.2));
  // world.set_block_at(0, 241, 0, "minecraft:diamond_ore").unwrap();
  // world.save().unwrap();
  // println!("Block ({:?}) = {:?}", p, world.get_block_at(p.0, p.1, p.2));
}

pub fn modify_test() {
  let mut world = World::new(WORLD_DIR);
  for x in 0..16 {
    for z in 0..16 {
      world.set_block_at(x, 240, z, "minecraft:polished_blackstone").unwrap();
    }
  }
  world.save().unwrap();
}

pub fn explore_block_states() {
  let mut w = World::new(WORLD_DIR);
  let r = w.get_region(REGION_X, REGION_Z).unwrap();
  r.load_chunk(0, 0).unwrap();
  let c = &r.get_chunk(0, 0).unwrap();
  let sections: Vec<&Tag> = c.level.sections.iter().map(|s| &s.section_nbt).collect();

  println!("Looking at chunk bounding {:?} -> {:?}", c.get_lower_bounds(), c.get_upper_bounds());
  println!("Sections ({})", sections.len());
  for (i, s) in sections.iter().enumerate() {
    println!("Section[{}]{}", i, s)
  }
}

pub fn main() {
  // explore_chunk_nbt_structure();
  world_test();
  // fix_modded_world::main();
}
