# NBT Fiddler

A simple minimal Rust library to read and write Minecraft files

It contains general purpose tools to read and modify data stored in Minecraft's [NBT format](https://minecraft.gamepedia.com/NBT_format). It also includes a library for modifying Minecraft world data like blocks and entities.

## Examples

TODO
